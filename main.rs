use std::fs::File;
use std::path::Path;
use std::io::prelude::*;
use std::io::{ BufRead , BufReader , Error };


/* El código esta incompleto. De momento lee el archivo, calcula e imprime el promedio.
   La idea es que ustedes hagan la parte de escritura en el nuevo archivo. Tienen que
   rescatar el nombre de cada alumno, y dependiendo de su promedio, escribir si estan
   aprobados o reprobados.*/

fn main() {

    /* El archivo que estoy usando no tiene los nombres de los alumnos, solo las notas. 
       En su archivo de notas a procesar debe estar el nombre de los alumnos, por lo que 
       deben desarrollar una forma de separar y guradar el nombre y además calcular el 
       promedio. Esta función solo calcula el promedio. */
    match leer_archivo("ejemplo.txt") {
        Ok(_) => println!("ok"),
        Err(_) => panic!(),
    }
    
}



fn leer_archivo ( nombre_archivo : &str ) -> Result <() , Error> {
    let file = File::open ( nombre_archivo )?;
    let reader = BufReader::new ( file ) ;

    for line in reader.lines () {
        let mut l = match line {
            Ok ( text ) => text,
            Err ( err ) => panic!(),
        };

        let mut suma : f32 = 0.0;

        // Revisar el archivo ejemplo.txt. Su archivo con las 
        // notas debe tener los nombres de los alumnos.
        let mut split = l.split(":"); 

        for item in split {
            let num : f32 = match item.trim().parse() {
                Ok(n) => n,
                Err(why) => panic!("error: {}", why),
            };
            suma += num;
        }

        let promedio :f32 = suma as f32 / 6 as f32;

        // Aquí en vez de imprimir el promedio deben guardar el resultado
        // para imprimirlos todos juntos en un nuevo archivo.
        println!("{}", promedio); 
    }

    return Ok (())
}